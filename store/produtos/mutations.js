export default {
  FETCH_PRODUTOS: (state, payload) => {
    state.produtos = []
    payload.forEach((produto) => state.produtos.push(produto))
  }
}
