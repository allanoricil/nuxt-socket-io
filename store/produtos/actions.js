export default {
  async getProdutosDaCarteira({ commit }, carteiraId) {
    await this.$axios
      .$get('/api/v1/ativo?carteiraId=' + carteiraId)
      .then((response) => {
        commit('FETCH_PRODUTOS', response)
      })
  }
}
