export default {
  FETCH_DARFS: (state, payload) => {
    state.darfs = []
    payload.forEach((darf) => state.darfs.push(darf))
  }
}
