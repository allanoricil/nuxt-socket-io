export default {
  getDarfs(state) {
    return JSON.parse(JSON.stringify(state.darfs))
  },
  getDarf: (state) => (id) => {
    return state.darfs.find((darf) => darf.id === id)
  }
}
