export default {
  getDarfsDaCarteira({ commit }, carteira) {
    this.$axios.$get('/api/v1/darf?carteiraId=' + carteira).then((response) => {
      commit('FETCH_DARFS', response)
    })
  },
  createDarfGenerationJob({ commit }, payload) {
    this.$axios
      .$post('/api/v1/darfGenerationJobs', payload)
      .then((response) => {
        return response
      })
  },
  getDarfGenerationJob({ commit }, { carteiraId, ano, mes }) {
    return this.$axios.$get(
      '/api/v1/darfGenerationJobs/search/findFirstByAnoAndMesAndCarteiraIdOrderByCreatedDateDesc?ano=' +
        ano +
        '&mes=' +
        mes +
        '&carteiraId=' +
        carteiraId
    )
  }
}
