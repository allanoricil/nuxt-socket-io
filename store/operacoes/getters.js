export default {
  getOperacoes(state) {
    return JSON.parse(JSON.stringify(state.operacoes))
  },
  getPrimeiraOperacaoDeVenda(state) {
    return state.operacoes.find((operacao) => {
      return operacao.compra === false
    })
  }
}
