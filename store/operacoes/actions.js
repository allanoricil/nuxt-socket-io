export default {
  async fetchOperacoesPorCarteira({ commit }, carteiraId) {
    await this.$axios
      .$get('api/v1/operacao?carteiraId=' + carteiraId)
      .then((response) => {
        commit('FETCH_OPERACOES', response)
      })
  },
  async fetchOperacoes({ commit }, { carteiraId, produtoId }) {
    console.log(carteiraId)
    console.log(produtoId)
    await this.$axios
      .$get(
        'api/v1/operacao?carteiraId=' + carteiraId + '&produtoId=' + produtoId
      )
      .then((response) => {
        commit('FETCH_OPERACOES', response)
      })
  },
  async fetchOperacoesPorProduto({ commit }, produtoId) {
    await this.$axios
      .$get('api/v1/operacao?produtoId=' + produtoId)
      .then((response) => {
        commit('FETCH_OPERACOES', response)
      })
  },
  async cadastraOperacaoNaCarteira({ commit }, operacao) {
    await this.$axios.$post('api/v1/operacao', operacao).then((response) => {
      commit('ADICIONA_OPERACAO', response)
    })
  }
}
