export default {
  FETCH_OPERACOES: (state, payload) => {
    state.operacoes = []
    payload.forEach((operacao) => state.operacoes.push(operacao))
  },
  ADICIONA_OPERACAO: (state, payload) => {
    state.operacoes.push(payload)
  }
}
