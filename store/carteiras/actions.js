export default {
  async fetchCarteiras({ commit }) {
    await this.$axios.$get('/api/v1/carteira').then((response) => {
      commit('FETCH_CARTEIRAS', response)
    })
  },
  async cadastraCarteira({ commit }, carteira) {
    await this.$axios.$post('/api/v1/carteira', carteira).then((response) => {
      commit('CADASTRA_CARTEIRA', response)
    })
  }
}
