/*const redis = require('redis')

const client = redis.createClient()

client.on('connect', () => {
  console.log('Connected to Redis')
})

client.on('erro', (err) => {
  console.log('Could not connect to Redis ' + err)
})

client.set('my test key', 'my test value', redis.print)
client.get('my test key', function(error, result) {
  if (error) {
    console.log(error)
    throw error
  }
  console.log('GET result -> ' + result)
})


client.subscribe()*/

const io = require('socket.io')(3000)
const redis = require('redis')
const redisAdaper = require('socket.io-redis')

io.adapter(redisAdaper({ host: 'localhost', port: 6379 }))

io.emit('hello', 'to all clients')
io.to('room42').emit('hello', "to all clients in 'room42' room")

io.on('connection', (socket) => {
  socket.on('message', function(data) {
    io.emit('send', data)
  })
})

const client = redis.createClient()
client.on('connect', () => {
  console.log('Connected to Redis')
})
client.once('ready', () => {
  console.log('READY')
})
